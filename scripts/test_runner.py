#! /usr/bin/env python

# Runs the server, then four copies of the command passed into the command line.
# Stupid script to make it easy to test. Feel free to use this as a base for
# something more complex.

import sys
import threading
import subprocess
import os.path

from mm18.server import server

def start_server(server_addr, server_port, game_log):
	server.game_log = game_log
	serve = server.ThreadedHTTPServer((server_addr, server_port), server.MMHandler)
	serve.allow_reuse_address = True
	server.server_instance = serve
	thread = threading.Thread(target=serve.serve_forever)
	thread.start()

def main(server_addr, server_port, command):
	# Start the server on the given port
	full_addr = server_addr + ":" + str(server_port)
	print "Starting the server on", full_addr
	start_server(server_addr, server_port, "log.txt")

	command.append(full_addr)

	# Start four clients
	for i in range(0,4):
		subprocess.Popen(command)

if __name__ == '__main__':
	if len(sys.argv) > 1:
		print "Running command " + str(sys.argv[1:])
	else:
		print "Error, need a command to run"
		sys.exit(1)

	main('localhost', 6969, sys.argv[1:])
